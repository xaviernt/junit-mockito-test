package com.test.junit.mockito.junit.mockito.test.entity;

import lombok.Data;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Data
@Embeddable
public class PersonEntityKey implements Serializable{
  
  private static final long serialVersionUID = 1L;
  
  @Id
  @Column(name = "PERSON_ID")
  private int personId;
  
}
