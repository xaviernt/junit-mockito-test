package com.test.junit.mockito.junit.mockito.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.test.junit.mockito.junit.mockito.test.model.PersonModel;
import com.test.junit.mockito.junit.mockito.test.service.PersonService;

@SpringBootTest
public class TestControllerTest {
	
	@Mock
	private PersonService personService;
	
	@InjectMocks
	private TestController testController;
	
	private List<PersonModel> allPersons;
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
		allPersons = new ArrayList<>();
		for(int i = 1; i <= 5; i++) {
			allPersons.add(generatePersonModel(i));
		}
	}
	
	@Test
	public void getAllPersonsTest() {
		Mockito.when(personService.getAllPersonIdSeparateForId()).thenReturn(allPersons);
		
		ResponseEntity<List<PersonModel>> response = testController.getAllPersons();
		
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertNotNull(response.getBody());
		Assertions.assertArrayEquals(allPersons.toArray(), response.getBody().toArray());
	}
	
	@Test
	public void getAllPersonsNotFoundTest() {
		Mockito.when(personService.getAllPersonIdSeparateForId()).thenReturn(new ArrayList<>());
		
		ResponseEntity<List<PersonModel>> response = testController.getAllPersons();
		
		Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		Assertions.assertFalse(response.hasBody());
	}
	
	@Test
	public void getOnePersonTest() {
		int id = 1;
		PersonModel person = generatePersonModel(id);
		Mockito.when(personService.getPersonById(Mockito.any(PersonModel.class))).thenReturn(person);
		
		ResponseEntity<PersonModel> response = testController.getOnePerson(generatePersonModel(id));
		
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		
		PersonModel personResponse = response.getBody();
		
		Assertions.assertAll("Same Person", () -> Assertions.assertEquals(person.getPersonId(), personResponse.getPersonId()),
				() -> Assertions.assertEquals(person.getName(), personResponse.getName()),
				() -> Assertions.assertEquals(person.getLastName(), personResponse.getLastName()),
				() -> Assertions.assertEquals(person.getStatus(), personResponse.getStatus()),
				() -> Assertions.assertEquals(person.getDate(), personResponse.getDate()) );
	}
	
	@Test
	public void getOnePersonNotFoundTest() {

		Mockito.when(personService.getPersonById(Mockito.any(PersonModel.class))).thenReturn(null);
		
		ResponseEntity<PersonModel> response = testController.getOnePerson(generatePersonModel(1));
		
		Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		Assertions.assertNull(response.getBody());
	}
	
	private PersonModel generatePersonModel(int id) {
		PersonModel person = new PersonModel();
		person.setPersonId(id);
		person.setName("name" + id);
		person.setLastName("lastName" + id);
		person.setStatus("00");
		person.setDate(id + "/01/1990");
		return person;
	}
}
