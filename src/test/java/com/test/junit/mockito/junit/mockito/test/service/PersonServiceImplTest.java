package com.test.junit.mockito.junit.mockito.test.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.test.junit.mockito.junit.mockito.test.component.UtilComponent;
import com.test.junit.mockito.junit.mockito.test.entity.PersonEntity;
import com.test.junit.mockito.junit.mockito.test.model.PersonModel;
import com.test.junit.mockito.junit.mockito.test.properties.ApplicationProperties;
import com.test.junit.mockito.junit.mockito.test.repository.PersonRepository;

@SpringBootTest
class PersonServiceImplTest {
	@Mock
	private PersonRepository personRepository;

	@Mock
	private UtilComponent utilComponent;
	
	@Mock
	private ApplicationProperties applicationProperties;
	
	@InjectMocks
	private PersonServiceImpl personServiceImpl;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		
	}

	@Test
	void testGetPersonById() {
		int id = 1;
		Mockito.when(personRepository.findByPersonId(Mockito.anyInt())).then(invocation -> {
			int idArgument  = invocation.getArgument(0);
			return generatePersonEntity(idArgument);
		});
		
		Mockito.when(utilComponent.mapper(Mockito.any(PersonEntity.class))).then(invocation -> {
			PersonEntity personArgument = invocation.getArgument(0);
			return generatePersonModel(personArgument.getPersonId());
		});
		
		PersonModel personExpected = generatePersonModel(id);
		PersonModel personResult = personServiceImpl.getPersonById(generatePersonModel(id));
		
		Assertions.assertNotNull(personResult);
		Assertions.assertAll("Same person", 
				() -> Assertions.assertEquals(personExpected.getPersonId(), personResult.getPersonId()),
				() -> Assertions.assertEquals(personExpected.getName(), personResult.getName()),
				() -> Assertions.assertEquals(personExpected.getLastName(), personResult.getLastName()),
				() -> Assertions.assertEquals(personExpected.getStatus(), personResult.getStatus()),
				() -> Assertions.assertEquals(personExpected.getDate(), personResult.getDate()) );
	}
	
	@Test
	void testGetPersonByIdNull() {
		int id = 1;
		Mockito.when(personRepository.findByPersonId(Mockito.anyInt())).thenReturn(null);
		
		PersonModel personResult = personServiceImpl.getPersonById(generatePersonModel(id));
		
		Assertions.assertNull(personResult);
	}

	@Test
	void testGetAllPersonIdSeparateForId() {
		List<PersonEntity> allPersons = new ArrayList<>();
		for(int i = 1; i <= 5; i++) {
			allPersons.add(generatePersonEntity(i));
		}
		
		Mockito.when(personRepository.findAll()).thenReturn(allPersons);
		
		Mockito.when(utilComponent.mapper(Mockito.any(PersonEntity.class))).then(invocation -> {
			PersonEntity personArgument = invocation.getArgument(0);
			return generatePersonModel(personArgument.getPersonId());
		});
		
		List<PersonModel> personsResult = personServiceImpl.getAllPersonIdSeparateForId();
		
		Assertions.assertEquals(allPersons.size(), personsResult.size());
		Assertions.assertAll(personsResult.stream().map(personResult -> () -> {
			PersonEntity personExpected = allPersons.get(personResult.getPersonId() - 1);
			Assertions.assertEquals(personExpected.getPersonId(), personResult.getPersonId());
			Assertions.assertEquals(personExpected.getName(), personResult.getName());
			Assertions.assertEquals(personExpected.getLastName(), personResult.getLastName());
			Assertions.assertEquals(personExpected.getStatus(), personResult.getStatus());
			Assertions.assertEquals(personExpected.getBirthDate(), personResult.getDate());
		}));
	}
	
	@Test
	void testGetAllPersonIdSeparateForIdNull() {
		
		Mockito.when(personRepository.findAll()).thenReturn(null);
		
		List<PersonModel> personsResult = personServiceImpl.getAllPersonIdSeparateForId();
		
		Assertions.assertTrue(personsResult.isEmpty());
	}

	private PersonModel generatePersonModel(int id) {
		PersonModel person = new PersonModel();
		person.setPersonId(id);
		person.setName("name" + id);
		person.setLastName("lastName" + id);
		person.setStatus("00");
		person.setDate(id + "/01/1990");
		return person;
	}
	
	private PersonEntity generatePersonEntity(int id) {
		PersonEntity person = new PersonEntity();
		person.setPersonId(id);
		person.setName("name" + id);
		person.setLastName("lastName" + id);
		person.setStatus("00");
		person.setBirthDate(id + "/01/1990");
		return person;
	}
	
}
